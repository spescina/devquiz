<?php

namespace Soisy;

use Soisy\Contracts\Matchable;
use Soisy\Exceptions\InvalidAmountException;
use Soisy\Exceptions\InvalidRatingException;

/**
 * Class Investment
 *
 * @package Soisy
 */
class Investment implements Matchable
{
    /**
     * @var int|float
     */
    protected $amount;
    /**
     * @var int|float
     */
    protected $rating;

    /**
     * Investment constructor.
     * @param int|float $amount
     * @param int|float $rating
     */
    public function __construct($amount, $rating)
    {
        $this->setAmount($amount);
        $this->setRating($rating);
    }

    /**
     * @return int|float
     */
    function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int|float
     */
    function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int|float $amount
     * @throws InvalidAmountException
     * @return void
     */
    function setAmount($amount)
    {
        if (!is_numeric($amount) || $amount <= 0) {
            throw new InvalidAmountException('The investment amount must be a positive number.');
        }

        $this->amount = $amount;
    }

    /**
     * @param int|float $rating
     * @throws InvalidRatingException
     * @return void
     */
    function setRating($rating)
    {
        if (!is_numeric($rating) || $rating < 1 || $rating > 5) {
            throw new InvalidRatingException('The investment rating must be a number between 1 and 5.');
        }

        $this->rating = $rating;
    }
}
<?php

namespace Soisy\Contracts;

/**
 * Interface Matchable
 *
 * @package Soisy\Contracts
 */
interface Matchable
{
    /**
     * @return int|float
     */
    function getAmount();

    /**
     * @return int|float
     */
    function getRating();

    /**
     * @param int|float $amount
     * @return void
     */
    function setAmount($amount);

    /**
     * @param int|float $rating
     * @return void
     */
    function setRating($rating);
}
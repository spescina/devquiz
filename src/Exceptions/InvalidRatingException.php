<?php

namespace Soisy\Exceptions;

/**
 * Class InvalidRatingException
 *
 * @package Soisy\Exceptions
 */
class InvalidRatingException extends \Exception
{

}
<?php

namespace Soisy\Exceptions;

/**
 * Class InvalidAmountException
 *
 * @package Soisy\Exceptions
 */
class InvalidAmountException extends \Exception
{

}
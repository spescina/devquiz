<?php

namespace Soisy;

/**
 * Class Matcher
 *
 * @package Soisy
 */
class Matcher
{
    /**
     * @var array
     */
    protected $loans;
    /**
     * @var array
     */
    protected $investments;
    /**
     * @var array
     */
    protected $matches;

    /**
     * Matcher constructor.
     */
    public function __construct()
    {
        $this->loans = [];
        $this->investments = [];
        $this->matches = [];
    }

    /**
     * @param Loan $loan
     * @return void
     */
    function addLoan(Loan $loan)
    {
        array_push($this->loans, $loan);
    }

    /**
     * @param Investment $investment
     * @return void
     */
    function addInvestment(Investment $investment)
    {
        array_push($this->investments, $investment);
    }

    /**
     * @return array
     */
    function getLoans()
    {
        return $this->loans;
    }

    /**
     * @return array
     */
    function getInvestments()
    {
        return $this->investments;
    }

    /**
     * @param Loan $loan
     * @param Investment $investment
     * @return void
     */
    function addMatch(Loan $loan, Investment $investment)
    {
        array_push($this->matches, [
            'loan'       => $loan,
            'investment' => $investment,
        ]);
    }

    /**
     * @return array
     */
    function getMatches()
    {
        return $this->matches;
    }

    /**
     * @return void
     */
    function run()
    {
        /** @var Loan $loan */
        foreach ($this->getLoans() as $loan) {

            /** @var Investment $investment */
            foreach ($this->getInvestments() as $investment) {

                if ($loan->getRating() !== $investment->getRating()) continue;
                if ($loan->getAmount() > $investment->getAmount()) continue;

                $this->addMatch($loan, $investment);
            }
        }
    }
}